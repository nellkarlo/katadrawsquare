﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            //  ------ DRAW SQUARE ------------

            Console.WriteLine("Enter a number for row*column: ");
            int row = Convert.ToInt32(Console.ReadLine());

            
            // multiply to offset difference in char width
            int column = Convert.ToInt32(row) * 2;

            string[,] square = new string[row, column];



            for (int x = 0; x < row; x++)
            {
                for (int y = 0; y < column; y++)
                {
                    if (x == 0 || x == row - 1 || y == 0 || y == column - 1)
                    {
                        Console.Write(String.Format("*", square[x, y]));
                    }
                    else
                    {
                        Console.Write(String.Format(" ", square[x, y]));
                    }
                }
                Console.WriteLine();
            }

            Console.ReadLine();

            // --------DRAW SQUARE ---------
        }
    }
}
